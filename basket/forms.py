from django import forms
from basket.models import Coach, Player, Team


class CoachForm(forms.ModelForm):
	class Meta:
		model = Coach
		fields = '__all__'

class PlayerForm(forms.ModelForm):
	class Meta:
		model = Player
		fields = '__all__'

class TeamForm(forms.ModelForm):
	class Meta:
		model = Team
		fields = '__all__'

