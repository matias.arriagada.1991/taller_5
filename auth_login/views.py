from django.shortcuts import render, redirect
from django.contrib import auth #authenticate, login, logout
from django.contrib import messages

def login(request):
	template_name = 'login.html'
	data = {}

	auth.logout(request)

	if request.POST:
		username = request.POST['username']
		password = request.POST['password']

		user = auth.authenticate(
			username=username,
			password=password
		)

		if user is not None:
			## flujo autenticado
			if user.is_active:
				## user valid
				auth.login(request, user)
				return redirect('basket:home')
			else:
				messages.add_message(
					request, 
					messages.ERROR, 
					'Incorrect username or password.'
				)

		else:
			messages.add_message(
				request, 
				messages.ERROR, 
				'Incorrect username or password.'
			)


	return render(request, template_name, data)


def logout(request):
	auth.logout(request)
	return redirect('auth:login')
